
" vi sux

set nocompatible
syntax off
filetype plugin indent off

inoremap <c-h> <Esc>:tabp<Enter>i
inoremap <c-l> <Esc>:tabn<Enter>i

set autoindent
set tabstop=8
set noexpandtab
set backspace=indent,eol,start
