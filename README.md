<h1 align="center">rice</h1>

<img src="plan9.png" alt="rice">

wm: xmonad

editor: [merlin](https://merlinfo.github.io) + [M](https://github.com/merlinfo/M) / vim

shell: bash / dash

terminal: alacritty

browser: brave

calc: [basil](https://github.com/geremachek/basil) / [rose](https://github.com/geremachek/rose)
